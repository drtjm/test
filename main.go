package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"
)

var help bool

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	return err == nil && !info.IsDir()
}

func readAirportLookup(filepath string) (map[string]string, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}
	if len(records) < 2 {
		return nil, fmt.Errorf("malformed")
	}
	headers := records[0]
	if len(headers) < 5 {
		return nil, fmt.Errorf("malformed")
	}
	lookup := make(map[string]string)
	for _, record := range records[1:] {
		if len(record) < 5 {
			return nil, fmt.Errorf("malformed")
		}
		name := record[0]
		icao := record[3]
		iata := record[4]
		if name == "" || icao == "" || iata == "" {
			return nil, fmt.Errorf("malformed")
		}
		lookup["##"+icao] = name
		lookup["#"+iata] = name
	}
	return lookup, nil
}

func processItinerary(input string, lookup map[string]string, datePattern, time12Pattern, time24Pattern *regexp.Regexp) string {
	var output strings.Builder
	scanner := bufio.NewScanner(strings.NewReader(input))
	blankLineCount := 0

	for scanner.Scan() {
		line := scanner.Text()
		if strings.TrimSpace(line) == "" {
			blankLineCount++
			if blankLineCount > 2 {
				continue
			}
		} else {
			blankLineCount = 0
		}
		line = replaceCodes(line, lookup)
		line = formatDatesAndTimes(line, datePattern, time12Pattern, time24Pattern)

		fmt.Println(line)
		output.WriteString(line + "\n")
	}
	return output.String()
}

func replaceCodes(line string, lookup map[string]string) string {
	for code, name := range lookup {
		line = strings.ReplaceAll(line, code, name)
	}
	return line
}

func formatDatesAndTimes(line string, datePattern, time12Pattern, time24Pattern *regexp.Regexp) string {
	line = datePattern.ReplaceAllStringFunc(line, func(s string) string {
		return formatDateString(datePattern.FindStringSubmatch(s)[1])
	})
	line = time12Pattern.ReplaceAllStringFunc(line, func(s string) string {
		return formatTimeString(time12Pattern.FindStringSubmatch(s)[1], true)
	})
	line = time24Pattern.ReplaceAllStringFunc(line, func(s string) string {
		return formatTimeString(time24Pattern.FindStringSubmatch(s)[1], false)
	})
	return line
}

func formatDateString(dateStr string) string {

	t, err := time.Parse(time.RFC3339, dateStr)
	if err != nil {
		return dateStr
	}
	return t.Format("02 Jan 2006")
}

func formatTimeString(timeStr string, is12Hour bool) string {

	t, err := time.Parse(time.RFC3339, timeStr)
	if err != nil {
		return timeStr
	}
	if is12Hour {
		return t.Format("03:04PM (-07:00)")
	}
	return t.Format("15:04 (-07:00)")
}

func helpFlag() {
	flag.BoolVar(&help, "h", false, "Show help")
}

func helpUsage() {
	fmt.Println("itinerary usage:\ngo run . ./input.txt ./output.txt ./airport-lookup.csv")
}

func main() {
	flag.Parse()
	if help || len(flag.Args()) != 3 {
		helpUsage()
		return
	}

	inputPath := flag.Arg(0)
	outputPath := flag.Arg(1)
	lookupPath := flag.Arg(2)

	if !fileExists(inputPath) {
		fmt.Println("Input not found")
		return
	}
	if !fileExists(lookupPath) {
		fmt.Println("Airport lookup not found")
		return
	}
	airportLookup, err := readAirportLookup(lookupPath)
	if err != nil {
		fmt.Println("Airport lookup malformed")
		return
	}
	inputData, err := os.ReadFile(inputPath)
	if err != nil {
		fmt.Println("Error reading input file")
		return
	}

	datePattern := regexp.MustCompile(`D\(([^)]+)\)`)
	time12Pattern := regexp.MustCompile(`T12\(([^)]+)\)`)
	time24Pattern := regexp.MustCompile(`T24\(([^)]+)\)`)

	processedData := processItinerary(string(inputData), airportLookup, datePattern, time12Pattern, time24Pattern)

	err = os.WriteFile(outputPath, []byte(processedData), 0644)
	if err != nil {
		fmt.Println("Error writing output file")
	}
}
